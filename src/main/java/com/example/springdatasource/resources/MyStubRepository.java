package com.example.springdatasource.resources;

import com.example.springdatasource.data.MyData;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component("stub")
@Qualifier("stub")
public class MyStubRepository implements MyRepository {
    @Override
    public List<MyData> findAll() {
        return Arrays.asList(new MyData(0L , "myData1"), new MyData(0L, "myData2"));
    }
}

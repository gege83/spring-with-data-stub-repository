package com.example.springdatasource.resources;

import com.example.springdatasource.data.MyData;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MyController {

    private MyRepository repository;

    public MyController(@Qualifier("getRepository") MyRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public List<MyData> getData(){
        return repository.findAll();
    }
}

package com.example.springdatasource.resources;

import com.example.springdatasource.data.MyData;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("real")
@Qualifier("real")
public interface MyRepository extends
        Repository<MyData, Long> {
    @Query(value = "select someData from MyData ", nativeQuery = true)
    List<MyData> findAll();
}

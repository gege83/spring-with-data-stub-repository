package com.example.springdatasource.resources;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpServletRequest;

@Configuration
public class MyConfiguration implements ApplicationContextAware {
    private HttpServletRequest request;
    private boolean enableStubs;
    private ApplicationContext applicationContext;

    public MyConfiguration(HttpServletRequest request, @Value("${enableStubs}") boolean enableStubs ) {
        this.request = request;
        this.enableStubs = enableStubs;
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public MyRepository getRepository() {
        String stub = request.getParameter("stub");
        boolean useStub = StringUtils.isEmpty(stub) || Boolean.parseBoolean(stub);
        String serviceName = useStub && enableStubs ? "stub" : "real";
        return applicationContext.getBean(serviceName, MyRepository.class);
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

package com.example.springdatasource.data;

import org.springframework.data.annotation.Id;

import javax.persistence.Entity;

@Entity
public class MyData {

    @Id
    @javax.persistence.Id
    private Long id;

    private String someData;

    public MyData() {
        //for JPA
    }

    public MyData(Long id, String someData) {
        this.id = id;
        this.someData = someData;
    }

    public String getSomeData() {
        return someData;
    }

    public void setSomeData(String someData) {
        this.someData = someData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
